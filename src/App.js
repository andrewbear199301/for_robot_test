import React, {useState, useEffect} from 'react';
import * as mqtt from 'mqtt';
// import HookMqtt from './components/Hook/'
// Hook or Class
// import ClassMqtt from './components/Class/'
// import './App.css';
import './css/bulma.min.css';
import './css/style.css';
import {Main, Navigation} from "./components";
import {Helper} from "./components/Helper";

function App() {

    // const host = '127.0.0.1'
    // const port = 1883
    //
    // const [client, setClient] = useState(null);
    // const mqttConnect = (host, mqttOption) => {
    //     // setConnectStatus( 'Connecting' );
    //     setClient(mqtt.connect(host, mqttOption));
    // };
    // // mqttConnect(host);
    // useEffect(() => {
    //     if (client) {
    //         console.log(client)
    //         client.on('connect', () => {
    //             // setConnectStatus( 'Connected' );
    //         });
    //         client.on('error', (err) => {
    //             console.error('Connection error: ', err);
    //             client.end();
    //         });
    //         client.on('reconnect', () => {
    //             // setConnectStatus( 'Reconnecting' );
    //         });
    //         client.on('message', (topic, message) => {
    //             const payload = {topic, message: message.toString()};
    //             // setPayload(payload);
    //         });
    //     }
    // }, [client]);
    return (
        <div className="App">
            <Navigation/>
            <section className="section">
                <Main/>
                <Helper/>
            </section>
            {/*<HookMqtt />*/}
            {/* Hook or Class */}
            {/* <ClassMqtt />*/}
        </div>
    );
}

export default App;
