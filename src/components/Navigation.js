import React from 'react';

class Navigation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {date: new Date(),
            dayList: ['日', '一', '二', '三', '四', '五', '六']};
    }
    //Lifecycle hooks
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: new Date()
        });
    }

    render() {
        return (
            <nav className="navbar">
                <div className="column is-offset-8">
                    <div className="title is-2 has-text-right "><span
                        className="clock-time">
                        {this.state.date.getHours()}:{this.state.date.getMinutes()}</span></div>
                    <div className="title is-2 has-text-right"><span className="clock-date"></span>&nbsp;
                        <span className="clock-day">星期{this.state.dayList[this.state.date.getDay()]}</span></div>
                </div>
            </nav>
        );
    }
}

export {Navigation};
