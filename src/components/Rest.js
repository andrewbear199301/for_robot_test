import React from 'react';
import {Link} from 'react-router-dom';
import {Image} from "antd";

const Rest = () => {

    return (
        <>
            <div className="container">
                <div className="title is-2 is-half has-text-centered">請選擇房型</div>
            </div>
            <section className="section main">
                <div className="tile is-ancestor">
                    <div className="tile is-12 is-vertical">
                        <div className="tile">
                            <Link className="box tile has-text-centered " to='/about'>
                                <span className="box-title ">三人房</span>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export
{
    Rest
}
    ;
