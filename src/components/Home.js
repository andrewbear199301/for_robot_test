import React from 'react';
import {Link} from 'react-router-dom';
import {Image} from "antd";

const Home = () => {

    return (
        <>
            <div className="container">
                <div className="title is-2 is-half has-text-centered">尚有空房</div>
            </div>
            <section className="section main">
                <div className="tile is-ancestor">
                    <div className="tile is-vertical is-6">
                        <div className="tile">
                            <div className="tile is-parent is-vertical">
                                <Link className="top-box box tile has-text-centered" to='/stay'>
                                    <Image src="img/sleep.png" className="box-img column is-6">
                                    </Image>
                                    <span className="box-title column is-6">住宿</span>
                                </Link>
                                <Link className="bottom-box box tile has-text-centered" to='/rest'>
                                    <Image src="img/check_in.png" className="box-img column is-6">
                                    </Image><span className="box-title column is-6">報到/補卡</span>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="tile is-vertical is-6">
                        <div className="tile">
                            <div className="tile is-parent is-vertical">
                                <Link className="top-box box tile has-text-centered" to='/about'>
                                    <Image src="img/rest.png" className="box-img column is-6">
                                    </Image>
                                    <span className="box-title column is-6">休息</span>
                                </Link>
                                <Link className="bottom-box box tile has-text-centered" to='/about'>
                                    <Image src="img/visit.png" className="box-img column is-6">
                                    </Image><span className="box-title column is-6">訪客</span>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export {Home};
