import React, {useState} from 'react';
import {Link, useLocation} from 'react-router-dom';
import Numpad from 'react-doge-numpad'
import 'react-doge-numpad/dist/index.css'

const StayPhone = () => {
    const location = useLocation()
    const {type} = location.state
    const [value, setValue] = useState("");


    return (
        <>

            <div className="container">
                <div className="title is-2 is-half has-text-centered">請輸入電話</div>
            </div>
            <section className="section main">
                <div className="tile is-ancestor">
                    <div className="tile is-12 is-vertical">
                        <div className="field is-expanded">
                            <div className="field has-addons">
                                <p className="control">
                                    <a className="button is-static">
                                        +886
                                    </a>
                                </p>
                                <p className="control is-expanded">
                                    <Numpad
                                        label="第一個０無需輸入"
                                        value={0}
                                        // inline={true}
                                        decimal={false}
                                        max={1000000000000}
                                        min={0}
                                        onChange={(value: number | string) => setValue(`${value}`)}>
                                        <input className="input " type="tel" value={value}/>
                                    </Numpad>
                                </p>
                            </div>
                            {/*<p className="help">Do not enter the first zero</p>*/}
                        </div>
                        <div className="tile">

                        </div>
                        <div className="tile">

                        </div>

                        <div className="tile is-parent">
                            <div className="tile is-3">
                            </div>
                            <div className="tile is-6">
                                <Link className="box tile has-text-centered " to='/stay/pay/no' state={{ type: 2 , phone: value }}>
                                    <span className="box-title ">確認</span>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export
{
    StayPhone
}
    ;
