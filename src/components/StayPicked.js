import React from 'react';
import {Link, useLocation} from 'react-router-dom';
import {Image} from "antd";
import axios from "axios";

const StayPicked = () => {
    return (
        <>
            <div className="container">
                <div className="title is-2 is-half has-text-centered">歡迎入住</div>
            </div>
            <section className="section main">
                <div className="tile is-ancestor">
                    <div className="tile is-12 is-vertical">

                        <div className="tile">
                            <div className="tile is-parent">
                                <Link className="box tile has-text-centered" to='/' >
                                    <Image src="img/welcome.png" className="box-img column is-6">
                                    </Image>
                                    <p></p>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export
{
    StayPicked
}
    ;
