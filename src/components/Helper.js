import React from 'react';
import {Link, useNavigate} from "react-router-dom";
import {Image} from "antd";

class Helper extends React.Component {
    render() {
        return (
            <section className="section helper">
                <div className="tile is-ancestor">
                    <div className="tile is-parent is-4">
                        <Link className="helper-box box is-child tile has-text-centered" to={-1}>
                            <Image src="img/back.png" className="box-img">
                            </Image>
                            <span className="helper-title">回上頁</span>
                        </Link>
                    </div>
                    <div className="is-parent tile is-4">
                    </div>
                    <div className="is-parent tile is-4">
                        <Link className="helper-box box tile is-child has-text-centered" to='/about'>
                            <Image src="img/service.png" className="box-img">
                            </Image>
                            <span className="helper-title ">我要幫助</span>
                        </Link>
                    </div>
                </div>
            </section>
        );
    }
}

export
{
    Helper
}
    ;
