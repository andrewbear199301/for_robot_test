import React from 'react';
import {Link, useLocation} from 'react-router-dom';
import {Image} from "antd";
import axios from "axios";

const StayPayType = () => {
    const location = useLocation()
    const { type } = location.state
    return (
        <>
            <div className="container">
                <div className="title is-2 is-half has-text-centered">支付方式</div>
            </div>
            <section className="section main">
                <div className="tile is-ancestor">
                    <div className="tile is-12 is-vertical">
                        <div className="tile">
                            <Link className="box tile has-text-centered " to='/stay/pay/cash' state={{ type: 2 }}>
                                <span className="box-title ">現金</span>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export
{
    StayPayType
}
    ;
