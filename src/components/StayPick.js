import React from 'react';
import {Link, useLocation} from 'react-router-dom';
import {Image} from "antd";
import axios from "axios";

const StayPick = () => {
    const location = useLocation()
    const { type } = location.state
    return (
        <>
            <div className="container">
                <div className="title is-2 is-half has-text-centered">取卡</div>
            </div>
            <section className="section main">
                <div className="tile is-ancestor">
                    <div className="tile is-12 is-vertical">

                        <div className="tile box is-vertical">
                            <p className="txt"></p>
                            <p className="txt">請取房卡後點選確認 剩餘時間：test</p>
                        </div>
                        <div className="tile is-parent">
                            <div className="tile is-3">
                            </div>
                            <div className="tile is-6">
                                <Link className="box tile has-text-centered " to='/stay/picked' state={{ type: 2 }}>
                                    <span className="box-title ">確認</span>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export
{
    StayPick
}
    ;
