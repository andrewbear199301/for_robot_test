import React from 'react';
import {Link} from 'react-router-dom';
import {Image} from "antd";
import axios from "axios";

const Stay = () => {
    // const baseURL = "https://idua.com.tw/api/K_getRoom";
    const current = formatDate();

    function formatDate() {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    // React.useEffect(() => {
    //     axios.get("http://idua-robot.test/api/test"
    //     ).then((response) => {
    //         console.log(response.data);
    //     });
    // }, []);
    return (
        <>
            <div className="container">
                <div className="title is-2 is-half has-text-centered">請選擇房型</div>
            </div>
            <section className="section main">
                <div className="tile is-ancestor">
                    <div className="tile is-12 is-vertical">
                        <div className="tile">
                            <div className="tile is-parent">
                                <Link className="box tile has-text-centered " to='/stay/price' state={{num: 3}}>
                                    <span className="box-title ">三人房</span>
                                </Link>
                            </div>
                            <div className="tile is-parent">
                                <Link className="box tile has-text-centered " to='/stay/price' state={{num: 6}}>
                                    <span className="box-title ">六人房</span>
                                </Link>
                            </div>
                        </div>
                        <div className="tile">
                            <div className="tile is-parent">
                                <Link className="box tile has-text-centered" to='/stay/price' state={{num: 4}}>
                                    <Image src="img/peo4.png" className="box-img column is-6">
                                    </Image>
                                    <span className="box-title column is-6">四人房</span>
                                </Link>
                            </div>
                        </div>
                        <div className="tile">
                            <div className="tile is-parent">
                                <Link className="box tile has-text-centered" to='/stay/price' state={{num: 2}}>
                                    <Image src="img/lover.png" className="box-img column is-6">
                                    </Image>
                                    <span className="box-title column is-6">雙人房</span>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export
{
    Stay
}
    ;
