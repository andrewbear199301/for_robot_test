import React from 'react';

import {Router, Routes, Route, useParams} from 'react-router-dom';

import {Home} from './Home';
import {Stay} from './Stay';
import {Rest} from './Rest';
import {StayPrice} from './StayPrice';
import {StayPhone} from './StayPhone';
import {StayPayNo} from './StayPayNo';
import mqtt from "mqtt";
import {StayPayType} from "./StayPayType";
import {StayPayCash} from "./StayPayCash";
import {StayPick} from "./StayPick";
import {StayPicked} from "./StayPicked";

const Main = () => {
    const INCOMING_TOPICS = [
        'action/accepted',
        'action/rejected',
        'task_result',
        'notification',
        'robot_state',

    ]
    // const client = mqtt.connect('ws://localhost:1884', {keepalive: 20})
    // console.log(client)
    // client.on('connect', function () {
    //     INCOMING_TOPICS.forEach((topic) => client.subscribe(topic))
    //     console.log(client)
    //     client.publish("command", JSON.stringify({ type: 'power_on_calibration' }));
    //     client.publish("command", JSON.stringify({ type: 'start_robot' }));
    // })
    // client.on('message', (topic, message) => {
    //     console.log(JSON.parse(message.toString()))
    // });
    // const handleParentFun = () =>{
    //     console.log('test')
    // }
    // const checkIn = (component) =>{
    //     client.publish("operation", JSON.stringify({ type: 'check_in' }));
    //     return component
    // }
    // const roomBooked = (component) =>{
    //     client.publish("event", JSON.stringify({ type: 'check_in/room_booked' }));
    //     console.log('roomBooked clicked')
    //     return component
    // }
    // const cardDropped = (component) =>{
    //     client.publish("event", JSON.stringify({ type: 'check_in/card_dropped' }));
    //     return component
    // }
    // const cardPicked = (component) =>{
    //     client.publish("event", JSON.stringify({ type: 'check_in/card_picked' }));
    //     return component
    // }

    return (

            <Routes>
                <Route exact path="/" element={<Home/>}/>
                {/*<Route exact path="/stay" element={checkIn(<Stay/>)}/>*/}
                <Route exact path="/stay" element={<Stay/>}/>
                <Route exact path="/stay/price" element={<StayPrice/>}/>
                {/*<Route exact path="/stay/pay/type" element={roomBooked(<StayPayType/>)}/>*/}
                <Route exact path="/stay/pay/type" element={<StayPayType/>}/>
                <Route exact path="/stay/pay/cash" element={<StayPayCash/>}/>
                <Route exact path="/stay/pay/no" element={<StayPayNo/>}/>
                <Route exact path="/stay/phone" element={<StayPhone/>}/>
                <Route exact path="/stay/pick" element={<StayPick/>}/>
                {/*<Route exact path="/stay/pick" element={cardDropped(<StayPick/>)}/>*/}
                <Route exact path="/stay/picked" element={<StayPicked/>}/>
                {/*<Route exact path="/stay/picked" element={cardPicked(<StayPicked/>)}/>*/}
                <Route exact path="/rest" element={<Rest/>}/>
            </Routes>
    );
}

export {Main};
