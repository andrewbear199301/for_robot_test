import React from 'react';
import {Link, useLocation} from 'react-router-dom';
import {Image} from "antd";
import axios from "axios";

const StayPrice = () => {
    const location = useLocation()
    // const { num } = location.state
    // const baseURL = "https://idua.com.tw/api/K_getRoom";
    const current = formatDate();

    function formatDate() {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }
    // React.useEffect(() => {
    //     axios.post(baseURL, {
    //         HotelId: "300",
    //         TerminalID: "T0000000",
    //         MerchantID: "000812770026607",
    //         CheckOut: current,
    //         Lan: "tw",
    //         ChooseType: "stay",
    //     }).then((response) => {
    //         console.log(response.data);
    //     });
    // }, []);
    return (
        <>
            <div className="container">
                <div className="title is-2 is-half has-text-centered">請選擇房型價錢</div>
            </div>
            <section className="section main">
                <div className="tile is-ancestor">
                    <div className="tile is-12 is-vertical">
                        <div className="tile">
                            <Link className="box tile has-text-centered " to='/stay/phone' state={{ type: 2 }}>
                                <span className="box-title ">1380</span>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export
{
    StayPrice
}
    ;
