import React from 'react';
import {Link, useLocation} from 'react-router-dom';

const StayPayNo = () => {
    const location = useLocation()
    const {type , phone} = location.state

    return (
        <>
            <div className="container">
                <div className="title is-2 is-half has-text-centered">確認入住明細</div>
            </div>
            <section className="section main">
                <div className="tile is-ancestor">
                    <div className="tile is-12 is-vertical">

                        <div className="tile box is-vertical">
                            <p className="txt">電話：0{phone}</p>
                            <p className="txt">房型：test</p>
                            <p className="txt">入房：test</p>
                            <p className="txt">退房：test</p>
                            <p className="txt">備註：test</p>
                            <p className="txt">已付：1380</p>
                            <p className="txt">未付：1380</p>

                        </div>
                        <div className="tile is-parent">
                            <div className="tile is-3">
                            </div>
                            <div className="tile is-6">
                                <Link className="box tile has-text-centered " to='/stay/pay/type' state={{ type: 2 }}>
                                    <span className="box-title ">確認</span>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export
{
    StayPayNo
}
    ;
